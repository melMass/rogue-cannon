import * as RE from 'rogue-engine';
// import * as CANNON from 'cannon-es';
import { Object3D } from 'three';

import CannonConfig from './CannonConfig';

export default class CannonComponent extends RE.Component {
  config: CannonConfig;

  awake() {
    this.setCannonConfig();
    // RE.Debug.log(`Getting config for ${this.name}`);
    if (!this.config){
      throw "Config not found in scene. Please add a CannonConfig object named 'Config'";
    }
  }
  protected setCannonConfig() {
    const config = RE.App.currentScene.getObjectByName("Config");

    if (config) {
      this.config = RE.getComponent(CannonConfig, config) as CannonConfig;
    }
  }


}
