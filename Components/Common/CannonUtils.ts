import CannonBody from '../Shapes/CannonBody';
import { Object3D } from 'three';
import * as RE from 'rogue-engine';

/**
 * Various utilities to interop from Three <-> Cannon
 */
export default class CannonUtils{

  static getBodyComponent(object3d: Object3D): CannonBody {
    const cannonBody = RE.getComponent(CannonBody, object3d);

    if (!cannonBody) {
      throw `${object3d.name} must have a Cannon Body Component`
    }

    return cannonBody;
  }
}
