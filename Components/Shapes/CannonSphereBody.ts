import * as RE from 'rogue-engine';
import * as CANNON from 'cannon-es';
import CannonBody from './CannonBody';
import { Box3 } from 'three';

const { Prop } = RE;

export default class CannonSphereBody extends CannonBody {
  shape: CANNON.Sphere;
  bbox: Box3;

  @Prop("Number") radiusOffset: number = 1;

  /**Cannon uses a perfect sphere so this is just for the editor preview */
  @Prop("Number") previewSegments: number = 10;

  static interface = {...CannonBody.superInterface, ...CannonSphereBody.interface};

  protected createShape() {
    this.bbox = new Box3().setFromObject(this.object3d);

    const bbox = this.bbox;

    const xDiff = (bbox.max.x - bbox.min.x);
    const yDiff = (bbox.max.y - bbox.min.y);
    const zDiff = (bbox.max.z - bbox.min.z);

    const maxSide = Math.max(xDiff, yDiff, zDiff)

    this.shape = new CANNON.Sphere(
      this.radiusOffset * (maxSide/2)
    );

  }
}

RE.registerComponent(CannonSphereBody);
