import * as RE from 'rogue-engine';
import { Vector3, Quaternion } from 'three';
import * as CANNON from 'cannon-es';
import CannonComponent from '../Common/CannonComponent';

export default class CannonBody extends CannonComponent {
  shape: CANNON.Shape;
  body: CANNON.Body;

  angularDamping = 0;
  linearDamping = 0;
  mass = 1;
  angularVelocity: Vector3 = new Vector3(0, 0, 0);

  positionOffset: Vector3 = new Vector3(0, 0, 0);

  bodyType: string;
  bodyTypeOptions: string[] = ["Dynamic", "Kinematic", "Static", "Sleepy", "Sleeping"]

  static superInterface = {
    "bodyType": "Select",
    "angularVelocity": "Vector3",
    "angularDamping": "Number",
    "linearDamping": "Number",
    "mass": "Number",
    "positionOffset": "Vector3"
  }

  awake() {
    super.awake();
    this.createShape();
    this.createBody();
    RE.Debug.log(`Created body ${this.name} (${this.bodyType})`)
  }

  start() {
    this.config.world.addBody(this.body);
  }

  update() {
    this.updatePhysics();
  }

  onBeforeRemoved() {
    this.config.world.removeBody(this.body);
  }

  protected createBody() {
    this.body = new CANNON.Body({
      angularDamping: this.angularDamping,
      linearDamping: this.linearDamping,
      mass: this.mass
    });

    this.body.addShape(this.shape);

    switch(this.bodyType){
      case "0":{
        this.body.type = CANNON.Body.DYNAMIC;
        break;
      }
      case "1":{
        this.body.type = CANNON.Body.KINEMATIC;
        break;
      }
      case "2":{
        this.body.type = CANNON.Body.STATIC;
        break;
      }
      case "3":{
        this.body.type = CANNON.Body.SLEEPY;
        break;
      }
      case "4":{
        this.body.type = CANNON.Body.SLEEPING;
        break;
      }
    }
    // RE.Debug.log(`body: ${typeof(this.bodyType)}`)


    this.copyObjectTransform();

    this.body.angularVelocity.set(this.angularVelocity.x, this.angularVelocity.y, this.angularVelocity.z);

  }

  protected createShape(): void {};

  protected copyObjectTransform() {
    const newPos = new CANNON.Vec3(
      this.object3d.position.x,
      this.object3d.position.y,
      this.object3d.position.z
    );

    const newQuaternion = new CANNON.Quaternion(
      this.object3d.quaternion.x,
      this.object3d.quaternion.y,
      this.object3d.quaternion.z,
      this.object3d.quaternion.w
    );

    this.body.quaternion.copy(newQuaternion);
    this.body.position.copy(newPos);
  }

  protected copyBodyTransform() {
    const newPos = new Vector3(
      this.body.position.x,
      this.body.position.y,
      this.body.position.z
    );

    const newQuaternion = new Quaternion(
      this.body.quaternion.x,
      this.body.quaternion.y,
      this.body.quaternion.z,
      this.body.quaternion.w
    );

    this.object3d.position.copy(newPos);
    this.object3d.quaternion.copy(newQuaternion);
  }

  private updatePhysics() {
    this.body.angularDamping = this.angularDamping;
    this.body.linearDamping = this.linearDamping;
    this.body.mass = this.mass;

    this.copyBodyTransform();
  }
}
