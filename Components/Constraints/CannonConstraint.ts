import * as RE from 'rogue-engine';
import { Object3D } from 'three';
import * as CANNON from 'cannon-es';
import CannonBody from '../Shapes/CannonBody';
import CannonComponent from '../Common/CannonComponent';

export default class CannonConstraint extends CannonComponent {
  constraint: CANNON.Constraint;
  targetBody: CANNON.Body;

  start() {
    this.createConstraint();
  }

  protected createConstraint() {}

  onRemoved() {
    this.config.world.removeConstraint(this.constraint);
  }
}
