import * as RE from 'rogue-engine';
import * as CANNON from 'cannon-es';
import CannonBody from '../Shapes/CannonBody';
import CannonComponent from '../Common/CannonComponent';

const { Prop } = RE;

export default class CannonMaterial extends CannonComponent {
  material: CANNON.Material;

  @Prop("Number") friction: number;
  @Prop("Number") restitution: number;

  awake() {
    super.awake();
    this.createMaterial();
  }

  start() {
    this.setMaterial();
  }

  protected createMaterial() {
    this.material = new CANNON.Material(this.name);

    // if (this.friction < 0)
      this.material.friction = this.friction;
    // if (this.restitution < 0)
    this.material.restitution = this.restitution;

    this.config.world.addMaterial(this.material);
  }

  private setMaterial() {
    const cannonBody = RE.getComponent(CannonBody, this.object3d);

    if (cannonBody) {
      cannonBody.shape.material = this.material;
    }
  }
}

RE.registerComponent(CannonMaterial);
