import * as RE from 'rogue-engine';
import * as CANNON from 'cannon-es';
import CannonBody from '../Shapes/CannonBody';
import CannonComponent from '../Common/CannonComponent';

export default class SetCannonMaterial extends CannonComponent {
  material: CANNON.Material;

  @RE.Prop("String") materialName: string;

  start() {
    this.setMaterial();
  }

  private getMaterial() {
    return this.config.world.materials.find(material => material.name === this.materialName)
  }

  private setMaterial() {
    const material = this.getMaterial();

    if (!material) return;

    this.material = material;

    const cannonBody = RE.getComponent(CannonBody, this.object3d);

    if (cannonBody) {
      cannonBody.shape.material = this.material;
    }
  }
}

RE.registerComponent(SetCannonMaterial);
